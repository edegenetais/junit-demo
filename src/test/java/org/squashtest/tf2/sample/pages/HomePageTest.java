package org.squashtest.tf2.sample.pages;

import org.junit.Assert;
import org.junit.Test;

public class HomePageTest{

	@Test
	public void theAnswerIs42(){
            Assert.assertEquals(42, 21*2);
	}

        @Test
        public void thisOneFails() {
            Assert.assertEquals("Earth", "flat");
        }
}
