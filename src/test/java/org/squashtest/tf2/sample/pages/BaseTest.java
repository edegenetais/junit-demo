package org.squashtest.tf2.sample.pages;

import org.junit.Assert;
import org.junit.Test;

/**
 * Base class for all the test classes
 * 
 * @author qtran
 */
public class BaseTest {
    @Test
    public void oneThatWorks(){
        Assert.assertNotSame("Vessie","Lanterne");
    }
    
    @Test
    public void oneThatDoesnt(){
        Assert.assertEquals("Map", "Territory");
    }
}
