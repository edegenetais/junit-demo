package org.squashtest.tf2.sample.pages;

import org.junit.Test;

/**
 * This junit test payload is used by the junit provider integration test.
 * @author edegenetais
 */
public class JunitPayLoadTest {
    @Test
    public void verboseJunitTest(){
        for(int i=0;i<=1024*1024;i++){
            System.out.println("This log is mighty verbose !!! line "+i);
        }
    }
}
